# Rolling Glory BE Test

## Tech spec
<p>a. PHP Laravel Framework 8</p>
<p>b. MySQL</p>
<p>c. sanctum authenticator </p>
		

## How to Set Up
<p>a. Clone Repository dengan link : git clone https://prasastiatia@bitbucket.org/prasastiatia/rolling-glory-test.git didalam folder htdocs di localhost (XAMPP)</p>
<p>b. nyalakan XAMPP (Apache & MySQL) </p>
<p>c. Buka link http://localhost/phpmyadmin dan buat database bernama : gift_be</p>
<p>d. import database bernama gift_be.sql ke dalam local dengan nama database : gift_be</p>
<p>e. Ubah nama folder "vendor" menjadi "vendors</p>
<p>f. Ubah nama file ".env-example" menjadi ".env"</p>
<p>g. Jalankan script : php artisan serve</p>
<p>h. Route API dapat diakses di : http://localhost:8000/api/gifts/ </p>
<p>i. Import postman collection yang terdapat pada folder docs</p>