<?php
   
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use Validator;
use App\Models\Gift;
use App\Http\Resources\gifts as GiftResource;
   
class GiftController extends BaseController
{

    public function index()
    {
        $gifts = Gift::all();
        return $this->sendResponse(GiftResource::collection($gifts), 'Gifts fetched.');
    }
    
    public function store(Request $request)
    {
        $input = $request->all();
        $validator = Validator::make($input, [
            'name' => 'required',
            'description' => 'required',
            'stok' => 'required'
        ]);
        if($validator->fails()){
            return $this->sendError($validator->errors());       
        }
        $gift = Gift::create($input);
        return $this->sendResponse(new GiftResource($gift), 'Gift created.');
    }
   
    public function show($id)
    {
        $gift = Gift::find($id);
        if (is_null($gift)) {
            return $this->sendError('Gift does not exist.');
        }
        return $this->sendResponse(new GiftResource($gift), 'Gift fetched.');
    }
 
    public function update(Request $request, Gift $gift)
    {
        $gift = Gift::findOrFail($gift->id);
        $gift->update([
            'name'     => $request->name,
            'description'     => $request->description,
            'slug'   => $request->slug,
            'stok' => $request->stok
        ]);
        
        
        return $this->sendResponse(new GiftResource($gift), 'Gift updated.');
    }

   
    public function destroy(Gift $gift)
    {
        $gift->delete();
        return $this->sendResponse([], 'Gift deleted.');
    }

    public function redeem(Request $request, Gift $gift)
    {
        $gift = Gift::findOrFail($gift->id);
        if($request->point == 200000)
        {
            $stok = $gift->stok - 1;
            $gift->update([
                'point' => $request->point,
                'stok' =>$stok
            ]);
        }
        
        
        return $this->sendResponse(new GiftResource($gift), 'Redeem Gift Succeed.');
    }

    public function rating(Request $request, Gift $gift)
    {
        $gift = Gift::findOrFail($gift->id);
        
            $rating = round($request->rating);
            $gift->update([
                'rating' =>$rating
            ]);
    
        
        
        return $this->sendResponse(new GiftResource($gift), 'Rating Gift Succeed.');
    }
}